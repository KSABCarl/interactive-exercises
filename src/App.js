import React, { Component } from 'react';
import 'whatwg-fetch';

import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom';

import { authCred, appstore } from './common/appstore';

import '../node_modules/ks-widgets-common/lib/styles.css';
import './common/style.css';

import { Package as WE } from './components/WordExcercises';
import { Package as TE } from './components/TextExcercises';

import { MainMenu } from './common/ui.js';
import { Home } from './common/home.js';

const gapi = window.gapi;

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: false
    };
    appstore.subscribe(() => {
      this.setState({
        loggedIn: appstore.getState().user.loggedIn
      });
    });
  }

  updateUser(name, email) {
    appstore.dispatch({type: 'AUTH_SET_USER', name, email});
    this.setState({loggedIn: true});
  }

  componentDidMount() {
    const self = this;

    if (gapi === undefined) {
      this.updateUser('John Doe', 'john@doe.com');
      return;
    }
    gapi.load('auth2', function() {
      gapi.auth2.init(authCred).then(function(auth) {
        if (auth.isSignedIn.get()) {
          const profile = auth.currentUser.get().getBasicProfile();

          self.updateUser(profile.getName(), profile.getEmail());
        }
      });
    });
  }

  disconnect() {
    const self = this;

    gapi.load('auth2', function() {
      gapi.auth2.init(authCred).then(function(auth) {
        if (auth.isSignedIn.get()) {
          auth.currentUser.get().disconnect().then(() => {
            appstore.dispatch({type: 'AUTH_LOGOUT'});
            self.setState({loggedIn: false});
          });
        }
      });
    });
  }

  login() {
    // handle if popup blocked!
    const self = this;

    appstore.dispatch({type: 'AUTH_LOGIN'});
    gapi.load('auth2', function() {
      gapi.auth2.init(authCred).then(function(auth) {
        if (auth.isSignedIn.get()) {
          const profile = auth.currentUser.get().getBasicProfile();

          self.updateUser(profile.getName(), profile.getEmail());
        } else {
          auth.signIn().then(function() {
            const profile = auth.currentUser.get().getBasicProfile();

            self.updateUser(profile.getName(), profile.getEmail());
          });
        }
      }, function(error) {
        console.log(error);
      });
    });
  }

  render() {
    const state = appstore.getState();
    const actions = {
      logout: this.disconnect.bind(this),
      login: this.login.bind(this)
    };
    const routes = this.state.loggedIn ? [WE, TE] : '';

    return (
      <div>
        <MainMenu signedIn={this.state.loggedIn} user={state.user.user} actions={actions} />
        <Router>
          <Switch>
            <Route exact path='/' component={props => <Home signedIn={this.state.loggedIn} {...props} />}/>
            {routes}
            <Route component={props => <Home signedIn={this.state.loggedIn} {...props} />}/>
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
