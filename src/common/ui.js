import React from 'react';
import PropTypes from 'prop-types';

import {
  Menu,
  Dropdown
} from 'semantic-ui-react';

const MainMenu = (props) => (
  <Menu stackable color='ks-purple' inverted>
    <Menu.Item>
      <a href='/'><img width='30em' alt='logo' src='ks-star.png' className='invert' /></a>
    </Menu.Item>
    <Menu.Menu position='right'>
      {(() => {
        if (props.signedIn) {
          return (
            <Dropdown item text={props.user.name}>
              <Dropdown.Menu>
                <Dropdown.Item>{props.user.email}</Dropdown.Item>
                <Dropdown.Item onClick={props.actions.logout}>Log out</Dropdown.Item>
                <Dropdown.Item>Stats</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>);
        }
        return <Menu.Item name='sign-int' onClick={props.actions.login}>Sign-in</Menu.Item>;
      })()}
    </Menu.Menu>
  </Menu>
);

MainMenu.propTypes = {
  signedIn: PropTypes.bool,
  actions: PropTypes.object
};

export { MainMenu };
