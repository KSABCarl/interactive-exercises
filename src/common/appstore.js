import { createStore, combineReducers } from 'redux';

const authCred = {
  'apiKey': 'AIzaSyAHZg5CuhT0I4M1rQq8Oj5KtfVNPI5Eo9Q',
  'clientId': '777473420216-fiolf865f8plkhn13r57u1ck40dnt9sa.apps.googleusercontent.com',
  'scope': 'profile'
};

const userReducer = (state = {}, action) => {
  switch (action.type) {
    case 'AUTH_LOGIN':
      return Object.assign(state, {
        loggingIn: true,
        loggedIn: false
      });

    case 'AUTH_SET_USER':
      return Object.assign(state, {
        loggingIn: false,
        loggedIn: true,
        user: {
          name: action.name,
          email: action.email
        }
      });

    case 'AUTH_LOGOUT':
      return Object.assign(state, {
        loggedIn: false,
        user: null
      });

    default:
      return state;
  }
};

const reducers = combineReducers({
  user: userReducer
});

let appstore = createStore(reducers);

export { authCred, appstore };
