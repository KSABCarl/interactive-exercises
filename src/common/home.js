import React from 'react';
import PropTypes from 'prop-types';
import 'whatwg-fetch';

import {
  Link
} from 'react-router-dom';

import {
  Header,
  Segment
} from 'semantic-ui-react';

import '../../node_modules/ks-widgets-common/lib/styles.css';
import './style.css';

const Home = (props) => (
  <div>
    <Header as='h1' icon textAlign='center'>
      <i className='icon'><img alt='logo' width='160 em' src='ks-star.png' /></i>
      Interactive Excercises
      <Header.Subheader>
        Kunskapsskolan i Sverige AB
      </Header.Subheader>
    </Header>
    {(() => {
      if (props.signedIn) {
        return (
          <Segment>
            WordExcercises:
            <Link to='/words/fr_gr_13_2.xml'>Fr gr</Link> |
            <Link to='/words/fr_gy_3_3_1.xml'>Fr gy</Link> |
            <Link to='/words/glosor.json'>Sp gy</Link>
          </Segment>
        );
      }
      return <p>Logga in först...</p>;
    })()}
    <hr />
  </div>
);

Home.propTypes = {
  signedIn: PropTypes.bool
};
export { Home };
