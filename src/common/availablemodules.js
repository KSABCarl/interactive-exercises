const modules = {
  'WordExcercises': [
    {
      module: 'WordListing',
      link: 'list',
      badge: true,
      header: 'showList',
      desc: 'descListing'
    },
    {
      module: 'Hangman',
      link: 'hangman',
      badge: true,
      header: 'playHangman',
      desc: 'descHangman'
    },
    {
      module: 'WordEditor',
      link: 'editor',
      badge: true,
      header: 'editWords',
      desc: 'descWords'
    }
  ],
  'TextExcercises': [
    {
      module: 'TextEditor',
      link: 'edit',
      badge: true,
      header: 'editText',
      desc: 'descText'
    }
  ]
};

export { modules };
