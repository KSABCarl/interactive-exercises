import React, {Component} from 'react';
import PropTypes from 'prop-types';

import {
  Container,
  Button
} from 'semantic-ui-react';

import { Link } from 'react-router-dom';

import { l10n } from 'ks-widgets-common';

class WordEditor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      words: props.getWords(),
      info: props.getInfo(),
      modLink: props.modLink()
    };
    console.log('wed-constr');
  }

  render() {
  //  const headerDest = this.state.info.course;
  //  const headerSrc = l10n[this.state.info.srcLang];
  //   const data = this.state.words;

    return (
      <Container text>
        <Link to={this.state.modLink}>
          <Button content={l10n.return} icon='reply' />
        </Link>
      </Container>
    );
  }
}

WordEditor.propTypes = {
  modLink: PropTypes.func,
  getWords: PropTypes.func,
  getInfo: PropTypes.func
};

export default WordEditor;
