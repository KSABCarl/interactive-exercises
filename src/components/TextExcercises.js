import React, {Component} from 'react';

import {
  Route,
  Switch,
  Link
} from 'react-router-dom';

import {
  Button,
  Card,
  Container,
  Header,
  Segment,
  List,
  Image
} from 'semantic-ui-react';

import { modules } from '../common/availablemodules';

import { l10n } from 'ks-widgets-common';
//
// import Hangman from './Hangman';
// import WordListing from './WordListing';
import TextEditor from './TextEditor';

const createList = (items, link) => {

    items = items.map(item => {
      return {
        link: link+item.link,
        badge: item.badge? require('./'+item.module+'/img/badge.png') : null,
        header: l10n[item.header],
        desc: l10n[item.desc],
      }
    })
    console.log(items);

  const list = items.map((item, i) => (
      <Card key={i} raised={true} centered={true}>
        <Link to={item.link}><Image alt="" src={item.badge}/></Link>
        <Card.Content>
          <Card.Header>
            <Link to={item.link}>{item.header}</Link>
          </Card.Header>
          <Card.Description>
            {item.desc}
          </Card.Description>
        </Card.Content>
      </Card>
    ));
  return list;
}


const Info = (info) => {
  return (
    <List horizontal>
      <List.Item>
        <List.Content>
          <List.Header>Kurs</List.Header>
          {info.course}
        </List.Content>
      </List.Item>
      <List.Item>
        <List.Content>
          <List.Header>Steg</List.Header>
          {info.step}
        </List.Content>
      </List.Item>
      <List.Item>
        <List.Content>
          <List.Header>Skolform</List.Header>
          {info.level}
        </List.Content>
      </List.Item>
    </List>
  )
}


const Menu = (match) =>
{
  const link = match.modLink()+'/';

  const items = modules.TextExcercises;

  const list = createList(items, link);

  const info = Info(match.getInfo());

  return (
    <Container text>
    <Header as='h2' icon textAlign='center'>
    <i className="icon"><img alt="logo" width="160em" src="ks-star-words.png" /></i>
      <Header.Content>
        {l10n.wordExcercises}
      </Header.Content>
    </Header>

    <Segment raised>
      {info}
    </Segment>

      <Card.Group>
      {list}
      </Card.Group>
      <Link to="/">
        <Button content={l10n.startPage} icon='home' />
      </Link>
    </Container>
  )
}


class TextExcercises extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '<p>The white <span class="blank">duck|goose|gander</span> quacked loudly.</p>',
      info: {lang: 'en'},
      isLoading: false,
      src: props.match.params.datasrc
    }
    console.log('te-constr');


    // dataLoader(this.state.src).then(
    //   list => this.setState({words: list.words, info: list.info, isLoading: false})
    // )
  }

  getWords() {
    return this.state.words;
  }

  getInfo() {
    return this.state.info;
  }

  modLink() {
    return '/text/'+this.state.src;
  }

  render() {

    if (this.state.isLoading) {
      return (<div>Laddar...</div>);
    }

    const modProps = {
      getText: this.getWords.bind(this),
      getInfo: this.getInfo.bind(this),
      modLink: this.modLink.bind(this)
    };

    return (
        <div>
          <Switch>
            <Route exact path="/text/:datasrc/edit" component={props => <TextEditor {...modProps} {...props} />} />
            <Route exact path="/text/:datasrc" component={props => <Menu {...modProps} {...props} />} />
          </Switch>
        </div>
    )
  }
}

const Package =
    <Route path="/text/:datasrc" component={TextExcercises} />


export { Package };
