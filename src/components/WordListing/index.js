import React, {Component} from 'react';
import PropTypes from 'prop-types';

import _ from 'lodash';

import {
  Table,
  Container,
  Button
} from 'semantic-ui-react';

import { Link } from 'react-router-dom';

import { l10n } from 'ks-widgets-common';

class WordListing extends Component {
  constructor(props) {
    super(props);
    this.state = {
      words: props.getWords(),
      info: props.getInfo(),
      modLink: props.modLink(),
      column: null,
      sortOrder: 'ascending'
    };
    console.log('wl-constr');
  }

  handleSort = clickedColumn => () => {
    const column = this.state.column;
    const sortOrder = this.state.sortOrder;
    const words = this.state.words;

    if (column !== clickedColumn) {
      this.setState({
        column: clickedColumn,
        words: _.sortBy(words, [clickedColumn]),
        sortOrder: 'ascending'
      });

      return;
    }

    this.setState({
      words: words.reverse(),
      sortOrder: sortOrder === 'ascending' ? 'descending' : 'ascending'
    });
  }

  render() {
    const headerDest = this.state.info.course;
    const headerSrc = l10n[this.state.info.srcLang];
    const data = this.state.words;
    const sortOrder = this.state.sortOrder;
    const column = this.state.column;

    return (
      <Container text>
        <Table sortable celled fixed selectable>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell sorted={column === 'dest' ? sortOrder : null} onClick={this.handleSort('dest')}>
                {headerDest}
              </Table.HeaderCell>
              <Table.HeaderCell sorted={column === 'src' ? sortOrder : null} onClick={this.handleSort('src')}>
                {headerSrc}
              </Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {_.map(data, ({ dest, src, bas, verb, sma, genus }) => (
              <Table.Row key={dest}>
                <Table.Cell>{dest}</Table.Cell>
                <Table.Cell>{src}</Table.Cell>
              </Table.Row>
            ))}
          </Table.Body>
        </Table>
        <Link to={this.state.modLink}>
          <Button content={l10n.return} icon='reply' />
        </Link>
      </Container>
    );
  }
}

WordListing.propTypes = {
  modLink: PropTypes.func,
  getWords: PropTypes.func,
  getInfo: PropTypes.func
};

export default WordListing;
