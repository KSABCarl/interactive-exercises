import React from 'react';
import {
  List
} from 'semantic-ui-react';

const Info = (info) => {
  return (
    <List horizontal>
      <List.Item>
        <List.Content>
          <List.Header>Kurs</List.Header>
          {info.course}
        </List.Content>
      </List.Item>
      <List.Item>
        <List.Content>
          <List.Header>Steg</List.Header>
          {info.step}
        </List.Content>
      </List.Item>
      <List.Item>
        <List.Content>
          <List.Header>Skolform</List.Header>
          {info.level}
        </List.Content>
      </List.Item>
    </List>
  );
};

export { Info };
