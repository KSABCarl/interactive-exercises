import React from 'react';

import {
  Link
} from 'react-router-dom';

import {
  Button,
  Card,
  Container,
  Header,
  Segment
} from 'semantic-ui-react';

import { l10n } from 'ks-widgets-common';

import { modules } from '../../common/availablemodules';

import { createList } from './createList.js';
import { Info } from './info.js';

const Menu = (match) => {
  const link = match.modLink() + '/';

  const items = modules.WordExcercises;

  const list = createList(items, link);

  const info = Info(match.getInfo());

  return (
    <Container text>
      <Header as='h2' icon textAlign='center'>
        <i className='icon'><img width='160em' alt='logo' src='ks-star-words.png' /></i>
        <Header.Content>
          {l10n.wordExcercises}
        </Header.Content>
      </Header>

      <Segment raised>
        {info}
      </Segment>

      <Card.Group>
        {list}
      </Card.Group>
      <Link to='/'>
        <Button content={l10n.startPage} icon='home' />
      </Link>
    </Container>
  );
};

export { Menu };
