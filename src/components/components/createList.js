import React from 'react';
import {
  Card,
  Image
} from 'semantic-ui-react';

import {
  Link
} from 'react-router-dom';

import { l10n } from 'ks-widgets-common';

const createList = (items, link) => {
  items = items.map(item => {
    return {
      link: link + item.link,
      badge: item.badge ? require('../' + item.module + '/img/badge.png') : null,
      header: l10n[item.header],
      desc: l10n[item.desc]
    };
  });
  console.log(items);

  const list = items.map((item, i) => (
    <Card key={i} raised={1} centered={1}>
      <Link to={item.link}><Image alt='' src={item.badge}/></Link>
      <Card.Content>
        <Card.Header>
          <Link to={item.link}>{item.header}</Link>
        </Card.Header>
        <Card.Description>
          {item.desc}
        </Card.Description>
      </Card.Content>
    </Card>
  ));

  return list;
};

export { createList };
