import React, {Component} from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

import {
  Dimmer,
  Header,
  Icon,
  Grid,
  Segment,
  Button
} from 'semantic-ui-react';
import { Link } from 'react-router-dom';

import './style.css';

import { l10n, translit, state } from 'ks-widgets-common';

import {
  Question,
  GuessWord,
  LetterList,
  Hangmanimation,
  Graveyard
} from './components';

class Hangman extends Component {
  constructor(props) {
    super(props);
    const stateID = 'Hangman|' + props.modLink();

    let hasState = state.retrieve(stateID);

    if (hasState) {
      state.fromSaved = true;
      this.state = Object.assign(hasState, {
        getWords: props.getWords,
        getInfo: props.getInfo
      });
    } else {
      this.state = {
        getWords: props.getWords,
        getInfo: props.getInfo,
        modLink: props.modLink(),

        stateID: stateID,
        fromSaved: false,

        game: {
          words: [],
          currentIndex: 0,
          correctDesc: '',
          currentStep: 0,
          currentWord: [],
          correctWord: [],
          correctTrans: [],
          wrongLetters: [],
          crosses: []
        },

        graphics: {
          overlay: {
            word: false,
            restart: false
          },
          ratio: 1,
          width: 400,
          height: 700
        }
      };
    }
  }

  updateDimensions() {
    let graphics = this.state.graphics;

    graphics.width = this.container.clientWidth;
    graphics.height = this.container.clientHeight;
    graphics.ratio = this.container.clientHeight / 700;

    this.anim.setState(graphics);
    this.graves.setState(graphics);
    this.setState({graphics});
  }

  componentDidMount() {
    if (this.state.fromSaved === false) {
      this.initGame();
    }
    this.updateDimensions();

    window.addEventListener('keydown', this.handleKeyDown.bind(this));
    window.addEventListener('resize', this.updateDimensions.bind(this));
  }

  componentDidUnMount() {
    window.removeEventListener('keydown', this.handleKeyDown.bind(this));
    window.removeEventListener('resize', this.updateDimensions.bind(this));
  }

  initGame() {
    let words = this.state.getWords();
    let game = {
      words: _.shuffle(words),
      currentIndex: 0,
      correctDesc: '',
      currentStep: 0,
      currentWord: [],
      correctWord: [],
      correctTrans: [],
      wrongLetters: [],
      crosses: []
    };

    this.setState({game}, this.newWord);
    this.showOverlay('all', false);
  }

  restart() {
    console.log('Restart');
    console.log('TODO: Register stats');
    this.initGame();
  }

  newWord() {
    let game = this.state.game;

    console.log(game.currentStep);
    game.wrongLetters = [];
    game.chosenWord = game.words[game.currentIndex];
    game.correctTrans = Array.from(translit(game.chosenWord.dest).toLowerCase());
    game.correctWord = Array.from(game.chosenWord.dest);
    game.correctDesc = game.chosenWord.src;
    game.currentIndex += 1;
    game.currentWord = game.correctWord.map(function(l) {
      return l.toLowerCase() !== l.toUpperCase() ? '' : l;
    });
    this.setState({game});
  }

  showOverlay(which, val = true) {
    let graphics = this.state.graphics;
    var w;

    if (which === 'all') {
      for (w in graphics.overlay) {
        graphics.overlay[w] = val;
      }
    } else {
      graphics.overlay[which] = val;
    }
    this.setState({graphics});
  }

  doAction(val) {
    let game = this.state.game;

    if (game.currentWord.includes(val)) {
      console.log('TODO: Blink letter?');
    } else if (game.wrongLetters.includes(val)) {
      console.log('TODO: Blink letter?');
    } else if (game.correctTrans.includes(val)) {

      let keys = game.correctTrans.map(function(el, i) {
        if (el.toLowerCase() === val) return i;
        return undefined;
      }).filter(function(el) {
        return el !== undefined;
      });

      keys.forEach(function(i) {
        game.currentWord[i] = game.correctWord[i];
      });
      this.setState({game});
      if (!game.currentWord.includes('')) {
        if (game.currentIndex === game.words.length) {
          this.showOverlay('restart');
        } else {
          this.showOverlay('word');
        }
      }
    } else {
      game.currentStep = game.currentStep + 1;

      if (game.currentStep === 16) {
        let x = Math.round(Math.random() * 10) * 10;
        let y = Math.round(Math.random() * 10) * 5;
        let rot = Math.round(Math.random() * 14) - 7;

        game.crosses.push({x, y, rot});
        game.currentStep = 0;
      }

      game.wrongLetters.push(val);
      this.setState({game});
    }
  }

  handleKeyDown(e) {
    if (e.metaKey || e.ctrlKey) return;
    if (e.key === 'Enter' && this.state.graphics.overlay.word === true) {
      this.handleOverlay();
    }
    const val = e.key.toLowerCase();

    if (val.length !== 1 || val.toLowerCase() === val.toUpperCase()) {
      return;
    }
    e.preventDefault();
    if (this.state.graphics.overlay.word === false) {
      this.doAction(val);
    }
  }

  handleOverlay() {
    this.newWord();
    this.showOverlay('word', false);
  }

  render() {
    state.store(this.state.stateID, this.state);
    const {game, graphics, modLink} = this.state;

    return (
      <Grid columns={2} id='hangman'>
        <Grid.Row stretched>
          <Grid.Column width={10}>
            <div id='graphics' ref={(container) => {this.container = container;}}>
              <Hangmanimation ref={(anim) => {this.anim = anim;}} step={game.currentStep} />
              <Graveyard crosses={game.crosses} ref={(graves) => {this.graves = graves;}} />
            </div>
          </Grid.Column>
          <Grid.Column width={6}>
            <Segment id='banner'>{l10n.hangman}</Segment>
            <Segment>
              <Question word={game.correctDesc} status={{n: game.currentIndex, t: game.words.length}} />
              <GuessWord word={game.currentWord} />
            </Segment>
            <Segment><LetterList letters={game.wrongLetters} /></Segment>
            <Segment>
              <Link to={modLink}>Tillbaka</Link>
            </Segment>
          </Grid.Column>
        </Grid.Row>
        <Dimmer active={graphics.overlay.word} onClick={this.handleOverlay.bind(this)} page>
          <Header as='h2' icon inverted>
            <Icon name='heart' />
            {l10n.correct}
            <Header.Subheader>
              <span>{game.currentIndex} {l10n.of} {game.words.length}</span>
              <Segment inverted color='teal' size='big'>{game.correctDesc}</Segment>
              {l10n.translatesTo}
              <Segment inverted color='olive' size='big'>{game.correctWord.join('')}</Segment>
            </Header.Subheader>
          </Header>
        </Dimmer>

        <Dimmer active={graphics.overlay.restart} page>
          <Header as='h2' icon inverted>
            <Icon name='heart' />
            {l10n.completed}
            <Header.Subheader>
              <span>{game.currentIndex} {l10n.of} {game.words.length}</span>
              <Segment inverted color='teal' size='big'>{game.correctDesc}</Segment>
              {l10n.translatesTo}
              <Segment inverted color='olive' size='big'>{game.correctWord.join('')}</Segment>
              <Button onClick={this.restart.bind(this)}
                icon='repeat' content={l10n.playAgain} />
              <Link to='/'>
                <Button content={l10n.startPage} icon='home' />
              </Link>
            </Header.Subheader>
          </Header>
        </Dimmer>
      </Grid>
    );
  }
}

Hangman.propTypes = {
  modLink: PropTypes.func,
  getWords: PropTypes.func,
  getInfo: PropTypes.func
};

export default Hangman;
