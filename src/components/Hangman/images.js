export default {
  cross: require('./img/cross.png'),
  eyes: require('./img/eyes.gif'),
  mound: [
    require('./img/1-1.png'),
    require('./img/1-2.png'),
    require('./img/1-3.png'),
    require('./img/1-4.png'),
    require('./img/1-5.png'),
    require('./img/1-6.png')
  ],
  hang: [
    require('./img/2-1.png'),
    require('./img/2-2.png'),
    require('./img/2-3.png'),
    require('./img/2-4.png'),
    require('./img/2-5.png'),
    require('./img/2-6.png'),
    require('./img/2-7.png'),
    require('./img/2-8.png'),
    require('./img/2-9.png'),
    require('./img/2-10.png')
  ]
};
