import React from 'react';
import PropTypes from 'prop-types';

const GuessWord = (props) => (
  <div>
    <h3 id='guess'>
      {props.word.map((letter, i) => {
        if (letter === ' ') {
          return <span key={i} className='space' />;
        }
        return <span key={i}>{letter}</span>;
      }
      )}
    </h3>
  </div>
);

GuessWord.propTypes = {
  word: PropTypes.string
};

export { GuessWord };
