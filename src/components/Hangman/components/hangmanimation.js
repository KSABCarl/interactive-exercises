import React, {Component} from 'react';
import PropTypes from 'prop-types';

import images from '../images.js';

function applyRatio(ratio, len) {
  return Math.round(ratio * len) + 'px';
}

class Hangmanimation extends Component {

  constructor(props) {
    super(props);
    this.state = {
      ratio: 1,
      width: 400,
      height: 700
    };
  }

  render() {
    const ratio = this.state.ratio;
    const step = this.props.step;

    const middle = {
      x: this.state.width / 2,
      y: this.state.height / 2
    };

    let mound = {
      height: applyRatio(ratio, 632),
      img: (step > 6 ? 5 : step - 1)
    };
    let main = {
      height: applyRatio(ratio, 700),
      width: applyRatio(ratio, 600)
    };
    let offset = (middle.x + 27 * ratio) + 'px';
    let hang = {
      height: applyRatio(ratio, 259),
      bottom: applyRatio(ratio, 291),
      offset: offset
    };
    let eyes = {
      bottom: (632 * ratio - 0.1 * ratio * 300) + 'px',
      right: (middle.x + 242 * ratio) + 'px',
      height: (0.1 * ratio * 300) + 'px'
    };

    return (
      step > 5 ? (
        <div id='animation' style={{height: main.height, width: main.width}}>
          <img id='mound' alt='' src={images.mound[mound.img]} height={mound.height} />
          <img id='eyes' alt='' src={images.eyes} height={eyes.height}
            style={{bottom: eyes.bottom, right: eyes.right}} />
          <img id='hang' alt='' src={images.hang[(step - 6)]} height={hang.height}
            style={{bottom: hang.bottom, right: hang.offset}} />
        </div>
      ) : (
        <div id='animation' style={{height: main.height, width: main.width}}>
          <img id='mound' alt='' src={images.mound[mound.img]} height={mound.height} />
        </div>
      )
    );
  }
}

Hangmanimation.propTypes = {
  step: PropTypes.number
};

export { Hangmanimation };
