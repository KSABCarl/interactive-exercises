import React from 'react';

import images from '../images.js';

const Cross = (pos) => (
  <img alt='' src={images.cross} height={pos.height}
    style={{bottom: pos.y + '%', left: pos.x + '%', transform: 'rotate(' + pos.rot + 'deg)'}} />
);

export { Cross };
