import React from 'react';
import PropTypes from 'prop-types';

import { l10n } from 'ks-widgets-common';

const LetterList = (props) => (
  <div id='usedLetters'><p>{l10n.usedLetters}:<br/>
    {props.letters.map((letter, i) =>
      <span key={i}>{letter}</span>
    )}</p>
  </div>
);

LetterList.propTypes = {
  letters: PropTypes.array
};

export { LetterList };
