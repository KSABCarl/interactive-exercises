import React from 'react';
import PropTypes from 'prop-types';

import { l10n } from 'ks-widgets-common';

const Question = (props) => (
  <div>
    <div id='status'>{l10n.number} {props.status.n} {l10n.of} {props.status.t}</div>
    <h2>{l10n.hangnmanQuestion}</h2>
    <h3>{props.word}</h3>
  </div>
);

Question.propTypes = {
  status: PropTypes.object,
  word: PropTypes.string
};

export { Question };
