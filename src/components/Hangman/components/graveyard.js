import React, {Component} from 'react';
import PropTypes from 'prop-types';

import { Cross } from './cross.js';

function applyRatio(ratio, len) {
  return Math.round(ratio * len) + 'px';
}

class Graveyard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ratio: 1,
      width: 400,
      height: 700,
      crosses: props.crosses
    };
  }

  render() {
    const ratio = this.state.ratio;
    const crossheight = applyRatio(ratio, 80);
    const gravewidth = applyRatio(ratio, 400);
    const graveheight = applyRatio(ratio, 200);
    const crosslist = this.state.crosses.map((cross, i) => {
      return <Cross key={i} height={crossheight} rot={cross.rot} x={cross.x} y={cross.y} />;
    });

    return (
      <div id='graveyard' style={{width: gravewidth, height: graveheight}}>
        {crosslist}
      </div>
    );
  }
}

Graveyard.propTypes = {
  crosses: PropTypes.array
};

export { Graveyard };
