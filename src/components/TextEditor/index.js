import React, {Component} from 'react';

import {
  Container,
  Button
} from 'semantic-ui-react';

import HoveringMenu from './components/editor.js';
import { TextRender } from './components/textrender.js';

import { l10n } from 'ks-widgets-common';

class TextEditor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      html: ''
    };
    console.log('ted-constr');
  }

  updateRender(html) {
    this.setState({html: html});
  }

  render() {
    return (
      <Container text>
        <HoveringMenu onChange={this.updateRender.bind(this)}/>
        <hr />
        <TextRender text={this.state.html} />
        <Button content={l10n.return} icon='reply' />
      </Container>
    );
  }
}

//
// class TextRender extends Component {
//   constructor(props) {
//     super(props);
//     let html = props.text || '<p>The white <strong>duck|goose|gander</strong> quacked loudly</p>';
//     html = html.split(/\<\/?strong\>/).map((part, i) => {
//       if (i % 2 == 0) return part;
//       else return '<Blank answers={['+part.split('|').join(',')+']} />';
//     });
//     this.state = {
//       html: html.join('')
//     }
//
//   }
//
//   render() {
//
//     console.log('render')
//     console.log(this.state.html);
//     const output = <JsxParser
//       bindings={{}}
//       components={{ Blank }}
//       jsx={this.state.html}
//     />
//     return(
//       <div>
//       {output}
//       </div>
//     )
//   }
// }

// <p>The white <Blank
//   answer=['duck', 'goose', 'gander']
//   caseSensitive={false}
//   showAnswer={true}
//   onEnter={false}
// /> quacked loudly.</p>

export default TextEditor;
