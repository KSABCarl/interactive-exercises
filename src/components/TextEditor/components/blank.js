import React, {Component} from 'react';
import PropTypes from 'prop-types';

class Blank extends Component {
  constructor(props) {
    super(props);
    this.state = {
      answers: props.answers.split('|'),
      isCorrect: false
    };
  }

  checkAnswer(event) {
    const val = event.target.value;

    if (this.state.answers.indexOf(val) >= 0) {
      console.log('right answer');
      this.setState({isCorrect: true});
    } else {
      this.setState({isCorrect: false});
    }
  }

  render() {
    const style = {
      backgroundColor: this.state.isCorrect ? 'green' : 'white'
    };

    return (
      <input style={style} onChange={this.checkAnswer.bind(this)} />
    );
  }
}

Blank.propTypes = {
  answers: PropTypes.string
};

export { Blank };
