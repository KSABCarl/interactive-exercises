import React from 'react';
import PropTypes from 'prop-types';

import JsxParser from 'react-jsx-parser';

import { Blank } from './blank.js';

const TextRender = (props) => {
  let html = props.text || '<p>The white <strong>duck|goose|gander</strong> quacked loudly</p>';

  html = html.split(/<\/?strong>/).map((part, i) => {
    if (i % 2 === 0) return part;
    return '<Blank answers="' + part + '" />';
  });
  console.log(html.join(''));
  const output = (<JsxParser
    bindings={{}}
    components={{ Blank }}
    jsx={html.join('')}
  />);

  return (
    <div>
      {output}
    </div>
  );
};

TextRender.propTypes = {
  text: PropTypes.string
};

export { TextRender };
