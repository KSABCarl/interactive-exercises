import React, {Component} from 'react';
import PropTypes from 'prop-types';

import {
  Route,
  Switch
} from 'react-router-dom';

import { data } from 'ks-widgets-common';

import Hangman from './Hangman';
import WordListing from './WordListing';
import WordEditor from './WordEditor';

import { Menu } from './components/menu.js';

class WordExcercises extends Component {
  constructor(props) {
    super(props);
    this.state = {
      words: [],
      info: {},
      isLoading: true,
      src: props.match.params.datasrc
    };
    console.log('we-constr');

    data.load(this.state.src).then(
      list => this.setState({words: list.words, info: list.info, isLoading: false})
    );
  }

  getWords() {
    return this.state.words;
  }

  getInfo() {
    return this.state.info;
  }

  modLink() {
    return '/words/' + this.state.src;
  }

  render() {

    if (this.state.isLoading) {
      return (<div>Laddar...</div>);
    }

    const modProps = {
      getWords: this.getWords.bind(this),
      getInfo: this.getInfo.bind(this),
      modLink: this.modLink.bind(this)
    };

    return (
      <div>
        <Switch>
          <Route exact path='/words/:datasrc/list' component={props => <WordListing {...modProps} {...props} />} />
          <Route exact path='/words/:datasrc/hangman' component={props => <Hangman {...modProps} {...props} />} />
          <Route exact path='/words/:datasrc/editor' component={props => <WordEditor {...modProps} {...props} />} />
          <Route exact path='/words/:datasrc' component={props => <Menu {...modProps} {...props} />} />
        </Switch>
      </div>
    );
  }
}

WordExcercises.propTypes = {
  match: PropTypes.object
};

const Package =
  <Route path='/words/:datasrc' component={WordExcercises} />;

export { Package };
